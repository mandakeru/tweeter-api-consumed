class TweetsController < ApplicationController
  before_action :fetch_new_tweets, only: :index

  def most_relevants
    @tweets = Tweet.order(followers_count: :desc, retweet_count: :desc, favorite_count: :desc)
    render json: @tweets, status: 200
  end

  def most_mentions
    @users = User.order(mentions_count: :desc)
    tweets = 
      @users.map do |user|
        {
         "#{user.name}": [
            user.mentions.map do |mention|
              mention.tweet
            end
         ]
        }
      end

    render json: tweets, status: 200
  end

  private

  def fetch_new_tweets
    tweets = Tweets::List.process
  end
end
