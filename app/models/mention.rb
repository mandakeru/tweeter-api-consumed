# == Schema Information
#
# Table name: mentions
#
#  id          :bigint(8)        not null, primary key
#  hashtags    :string
#  name        :string
#  screen_name :string
#  urls        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  tweet_id    :integer
#  user_id     :integer
#
# Indexes
#
#  index_mentions_on_tweet_id  (tweet_id)
#  index_mentions_on_user_id   (user_id)
#

class Mention < ApplicationRecord
  belongs_to :tweet
  belongs_to :user
end
