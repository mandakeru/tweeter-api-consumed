# == Schema Information
#
# Table name: users
#
#  id             :bigint(8)        not null, primary key
#  mentions_count :integer          default(0)
#  name           :string
#  uuid           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class User < ApplicationRecord
  has_many :mentions
end
