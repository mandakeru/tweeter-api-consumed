# == Schema Information
#
# Table name: tweets
#
#  id              :bigint(8)        not null, primary key
#  favorite_count  :integer
#  followers_count :integer
#  link            :string
#  profile_link    :string
#  retweet_count   :integer
#  screen_name     :string
#  text            :text
#  tweet_date      :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_tweets_on_retweet_count  (retweet_count)
#  index_tweets_on_uuid           (uuid) UNIQUE
#

class Tweet < ApplicationRecord
  has_one :mention
end
