module Tweets
  class Compare
    def self.process(tweet, tweets)
      new.process(tweet, tweets)
    end

    def process(tweet, tweets)
      new_tweets = []
      tweet['statuses'].each do |t|
        if tweets.include? t['id']
          next
        else
          if !t['entities'].empty?
            t['entities']['user_mentions'].each do |mention|
              if mention['screen_name'] == 'locaweb'
                @user = User.find_by(uuid: t['user']['id'])
                if @user.nil?
                  @user = User.create!(
                    name: t['user']['name'],
                    uuid: t['user']['id']
                  )

                  tweet = Tweet.create!(
                    favorite_count: t['favorite_count'],
                    followers_count: t.dig('user', 'followers_count'),
                    link: nil,
                    profile_link: t.dig('user', 'url'),
                    retweet_count: t['retweet_count'],
                    screen_name: t.dig('user', 'name'),
                    text: t['text'],
                    tweet_date: t['created_at'],
                    uuid: t['id']
                  )
                  
                  new_mention = Mention.create!(
                    hashtags: t['entities']['hashtags'],
                    name: mention['name'],
                    screen_name: mention['screen_name'],
                    urls: t['entities']['urls'],
                    tweet_id: tweet.id,
                    user_id: @user.id
                  )

                  @user.mentions_count = (@user.mentions_count + 1)
                  @user.save!
                else
                  tweet = Tweet.create!(
                    favorite_count: t['favorite_count'],
                    followers_count: t.dig('user', 'followers_count'),
                    link: nil,
                    profile_link: t.dig('user', 'url'),
                    retweet_count: t['retweet_count'],
                    screen_name: t.dig('user', 'name'),
                    text: t['text'],
                    tweet_date: t['created_at'],
                    uuid: t['id']
                  )
                  
                  new_mention = Mention.create!(
                    hashtags: t['entities']['hashtags'],
                    name: mention['name'],
                    screen_name: mention['screen_name'],
                    urls: t['entities']['urls'],
                    tweet_id: tweet.id,
                    user_id: @user.id
                  )

                  @user.mentions_count = (@user.mentions_count + 1)
                  @user.save!
                end
              else
                next
              end
            end  
          end

          new_tweets << tweet
        end
      end

      new_tweets
    end
  end
end