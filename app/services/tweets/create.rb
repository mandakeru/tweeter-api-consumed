module Tweets
  class Create
    def self.process(params)
      new.process(params)
    end

    def process(params)
      tweet_response = {}
      tweet = Tweet.new(params)
      if tweet.save
        tweet_response = {
          tweet: tweet,
          status: 'success'
        }
      else
        tweet_response = {
          tweet: tweet,
          status: 'success'
        }
      end
    end
  end
end
