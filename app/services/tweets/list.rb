module Tweets
  class List
    def self.process
      new.process
    end

    def process
      tweets_from_db = Tweet.all.pluck(:uuid)
      fetch_tweets = Tweets::Fetch.process
      compare = Tweets::Compare.process(fetch_tweets, tweets_from_db)
    end
  end
end
