require 'net/http'

module Tweets
  class Fetch
    def self.process
      new.process
    end

    def process
      fetch_tweets
    end

    private

    def fetch_tweets
      uri = URI('http://tweeps.locaweb.com.br/tweep')
      req = Net::HTTP::Get.new(uri.request_uri)
      req['Username'] = ENV['HTTP_USERNAME']
      res = Net::HTTP.start(uri.hostname, uri.port) {|http|
        http.request(req)
      }

      JSON.parse(res.body)
    end
  end
end
