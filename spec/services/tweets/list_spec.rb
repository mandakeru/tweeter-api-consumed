require 'rails_helper'

RSpec.describe Tweets::List do
  subject { described_class.new }

  let(:tweets_response) do
    {
      statuses: [
        {
          entities: {
            urls: [], 
            hashtags: [], 
            user_mentions: [
              {
                screen_name: 'locaweb', 
                name: 'Locaweb', 
                id: 42, 
                id_str: 42, 
                indices: [8, 16]
              }
            ]
          },
        text: 'The SDD @locaweb  connect the bluetooth system so we can transmit the JBOD microchip!',
        metadata: {
          iso_language_code: 'pt', 
          result_type: 'recent'
        },
        retweet_count: 0,
        in_reply_to_status_id_str: '534512',
        id: '88114',
        retweeted: false,
        in_reply_to_user_id: '92689',
        favorite_count: 390,
        user: 
         {
          name: 'Dvm Walsh Magali',
          favourites_count: 17,
          url: nil,
          id: '149459',
          followers_count: 25,
          description: 'Born 330 Live 310',
          screen_name: 'dvm_walsh_magali'
        },
        in_reply_to_screen_name: 'ashleigh_fahey',
        source: 'web',
        in_reply_to_status_id: '534512'
      },
     {
      entities: {
        urls: [],
        hashtags: [],
        user_mentions: [
          {
            screen_name: 'grant_lakin_jr', 
            name: 'Grant Lakin Jr', 
            id: '134495', 
            id_str: '134495', 
            indices: [26, 41]
          }
        ]
      },
      text: 'Use the wireless IB micro @grant_lakin_jr ile interface!',
      retweet_count: 0,
      id: '608135',
      retweeted: false,
      in_reply_to_user_id: nil,
      favorite_count: 0,
      user: 
        {
          name: 'Ambrose Dibbert',
          id_str: '208078',
          entities: {
            url: {
              urls: [{
                expanded_url: nil, 
                url: nil, 
                indices: [0, 0]
              }]
            }, 
            description: {urls: []}
          },
          favourites_count: 42,
          url: nil,
          id: 208078,
          lang: 'en',
          followers_count: 954,
          time_zone: 'Pacific Time (US & Canada)',
          description: 'Born 330 Live 310',
          statuses_count: 579,
          friends_count: 954,
          screen_name: 'ambrose_dibbert'
        },
        in_reply_to_screen_name: 'kuvalis_kianna',
        source: 'web',
        in_reply_to_status_id: 237203
      }
    ]
  }
   
  end

  let(:parsed_response) { tweets_response.to_json }

  before do
    @tweets = FactoryBot.create_list(:tweet, 10)
    allow_any_instance_of(Tweets::Fetch).to receive(:process).and_return(JSON.parse(parsed_response))
  end


  describe '#process' do
    it 'lists the tweets' do
      response = subject.process
      expect(response.count).to eq(2)
    end
  end
end