# == Schema Information
#
# Table name: tweets
#
#  id              :bigint(8)        not null, primary key
#  favorite_count  :integer
#  followers_count :integer
#  link            :string
#  profile_link    :string
#  retweet_count   :integer
#  screen_name     :string
#  text            :text
#  tweet_date      :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_tweets_on_retweet_count  (retweet_count)
#  index_tweets_on_uuid           (uuid) UNIQUE
#

require 'rails_helper'

RSpec.describe Tweet, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
