# == Schema Information
#
# Table name: tweets
#
#  id              :bigint(8)        not null, primary key
#  favorite_count  :integer
#  followers_count :integer
#  link            :string
#  profile_link    :string
#  retweet_count   :integer
#  screen_name     :string
#  text            :text
#  tweet_date      :string
#  uuid            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_tweets_on_retweet_count  (retweet_count)
#  index_tweets_on_uuid           (uuid) UNIQUE
#

FactoryBot.define do
  factory :tweet do
    favorite_count { rand(0..30) }
    followers_count { rand(0..100) }
    link {"http://tweeps.locaweb.com.br/tweep/#{uuid}"}
    profile_link {"http://tweeps.locaweb.com.br/tweep/#{screen_name}"}
    retweet_count { rand(0..10)}
    screen_name Faker::GameOfThrones.character
    text Faker::GameOfThrones.quote
    tweet_date {Date.today}
    uuid { rand(1239..4590).to_s }
    
  end
end
