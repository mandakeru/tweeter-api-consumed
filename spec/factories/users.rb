# == Schema Information
#
# Table name: users
#
#  id             :bigint(8)        not null, primary key
#  mentions_count :integer          default(0)
#  name           :string
#  uuid           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryBot.define do
  factory :user do
    name Faker::Name.unique.name
    uuid { rand(1239..4590).to_s }
    mentions_count { rand(1..20).to_s }
  end
end
