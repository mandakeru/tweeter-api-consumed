require 'rails_helper'

RSpec.describe TweetsController, type: :controller do
  before do
    @tweets = FactoryBot.create_list(:tweet, 5)
    @user1 = FactoryBot.create(:user)
    @user2 = FactoryBot.create(:user)
    @user3 = FactoryBot.create(:user)
    @user4 = FactoryBot.create(:user)
  end

  describe '#most_relevants' do
    it 'returns the most relevants tweets' do
      get :most_relevants
      json = JSON.parse(response.body)
      expect(json.first['followers_count'] > json.second['followers_count']).to be_truthy 
    end
  end

  describe '#most_mentions' do
    xit 'returns the users with most mentions' do
      get :most_mentions
    end
  end
end
