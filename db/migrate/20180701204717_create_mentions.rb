class CreateMentions < ActiveRecord::Migration[5.2]
  def change
    create_table :mentions do |t|
      t.string :urls
      t.string :hashtags
      t.string :screen_name
      t.string :name
      t.integer :tweet_id
      t.integer :user_id

      t.timestamps
    end

    add_index :mentions, :tweet_id
    add_index :mentions, :user_id
  end
end
