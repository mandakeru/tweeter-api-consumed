class AddMentionsCountToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :mentions_count, :integer, default: 0
  end
end
