class CreateTweets < ActiveRecord::Migration[5.2]
  def change
    create_table :tweets do |t|
      t.string :screen_name
      t.integer :followers_count
      t.integer :retweet_count
      t.integer :favorite_count
      t.text :text
      t.string :tweet_date
      t.string :profile_link
      t.string :link
      t.string :uuid

      t.timestamps
    end

    add_index :tweets, :retweet_count
    add_index :tweets, :uuid, unique: true
  end
end
